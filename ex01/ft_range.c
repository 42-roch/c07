/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 18:54:28 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/13 10:11:44 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdlib.h>

int	*ft_range(int min, int max)
{
	int	a;
	int	i;
	int	*t;

	if (min >= max)
		return (0);
	t = malloc(sizeof(int) * (max - min));
	a = min;
	i = 0;
	while (i <= (max - min))
	{
		t[i] = a;
		a++;
		i++;
	}
	return (t);
}
