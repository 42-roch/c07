/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/12 19:12:57 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/13 11:41:22 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	getlenght(int **a)
{
	int	l;

	l = 0;
	while (a[l])
		l++;
	return (l);
}

int	ft_ultimate_range(int **range, int min, int max)
{
	int	i;

	if (min >= max)
	{
		*range = NULL;
		return (0);
	}
	*range = malloc(sizeof(int) * (max - min));
	if (!range)
		return (-1);
	i = 0;
	while (min < max)
	{
		*(*range + i) = min;
		i++;
		min++;
	}
	return (i);
}
